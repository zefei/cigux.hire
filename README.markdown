Cigux Hire
==========

This is the entry point for Cigux Inc. technical hiring. We are a startup based 
in L.A., focusing on __cloud and mobile solutions for business use__. We are 
hiring talented engineers who share our curiosity and love for challenges, so we 
devised this process which we hope wouldn't disappoint our future colleagues.  
It is a simple Git repositry, and should be treated so.

This repo contains several subfolders and files, all of which should be read 
before going further:

/info: info we want to gather from you.
/code: some sample code we provided, and we expect to see your code here.
/tools: tools we use to manage this repo, currently empty.

Applicants should make a private fork of this repo, then do whatever should be 
done considering the __context__. Keep in mind that as long as the fork is 
private, no one can access its content unless given permission.

Again, we'd like to repeat our hint: this is a public Git repo.
