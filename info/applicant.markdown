Applicant Infomation
====================

We want to hire the best engineers, but interviews are usually too short for us 
to identify real good talents, so we would like as much information as possible 
from you before that.

Tell us some history about you as a programmer, e.g. what projects you were 
__proudly__ involved, what you have learnt from those experiences, what kind of 
projects insterest you, what challenges you set for yourself, etc. Please note 
that we are genuinely interested in info about you, as a programmer or as a 
person, but not as an employee:


Tell us some more about what you like and what you use as a programmer, e.g.  
your favourite programming languages, editors, OSes, SCMs, even computers, etc.:


Tell us where we can look at your previous work, __this is most important__.  
Either some public repo like this one, or some sample codes placed in /code 
folder. Explain them a bit so we know where to look at and what you did:


Well, that's what we have so far. If there's anything you want to tell us more 
about, please go ahead. Also, if you want to know anything from us, just ask:


