// hint: this is Javascript, not Java

var Cigux = {
  Hire: {

    // find the maximal value in an array of numbers
    max: function(anArray) {
      var maxval = anArray[0];
      for (var i in anArray) {
        if (anArray[i] > maxval) maxval = anArray[i];
      }
      return maxval;
    }
  }
};

// test
console.log(Cigux.Hire.max([2, 5, 32, 1, -3, 0, 3245234, 234]));
