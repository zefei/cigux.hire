// hint: this is C++, not C; we use C++ very rarely

#include <vector>
#include <iostream>
#include <algorithm>

namespace cigux_hire
{
  // some good qsort implementation
  template <class RandomAccessIterator>
  void qsort(RandomAccessIterator first, RandomAccessIterator last) {
    if (first != last) {
      RandomAccessIterator pivot = first, left = first + 1, right = last;

      while (left != right) {
        if (*left <= *pivot) {
          left++;
        } else {
          right--;
          std::iter_swap(left, right);
        }
      }

      left--;
      std::iter_swap(first, left);

      qsort(first, left);
      qsort(right, last);
    }
  }

  // sort a std::vector of numbers into ascending order
  template <typename T>
  void sort(std::vector<T> &v) {
    qsort(v.begin(), v.end());
  }
}

// test
int main() {
  int ints[] = {2, 5, 32, 1, -3, 0, 3269, 234};
  std::vector<int> numbers(ints, ints + 8);
  cigux_hire::sort(numbers);

  std::vector<int>::iterator it;
  for (it = numbers.begin(); it != numbers.end(); it++) {
    std::cout << *it << " ";
  }
  std::cout << std::endl;

  return 0;
}
